package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"time"
)

// batchStep represents a single step of a batch, which requires N PVs of the trade set to be completed
type batchStep struct {
	Id int `json:"batch_step_id"` // Id of the batch step
	N  int `json:"number_pvs"`    // Number of PVs to calculate per trade
}

type group struct {
	Id      int     `json:"node_group_id"`       // Id of the group
	Q       int     `json:"node_count"`          // Number of nodes in the group
	Scaling float64 `json:"performance_scaling"` // Scaling factor (<= 1.0) compared to fastest
}

// tradeVariety provides the ability to model multiple trade types being sent in a batch to the grid
type tradeVariety struct {
	N            int     `json:"trade_count"`      // The number of this variety
	ShortestTime float64 `json:"shortest_pv_time"` // The least time to value this variety
	LongestTime  float64 `json:"longest_pv_time"`  // The most time to value this variety
}

// tradeId provides typing for a trade id
type tradeId int

// tradePVTimes holds the PV time for each of the trades in the trade set
type tradePVTimes map[tradeId]float64

// totalPV returns the time taken to calculate a PV of the whole trade set
func (t tradePVTimes) totalPV() float64 {
	var tot float64
	for _, v := range t {
		tot += v
	}
	return tot
}

// calculateTotalPVTime returns the total time taken to complete all the batch steps on the whole trade set
func (t tradePVTimes) calculateTotalPVTime(steps []batchStep) float64 {
	totalN := 0
	for i := range steps {
		totalN += steps[i].N
	}
	return t.totalPV() * float64(totalN)
}

// tradeJSON is used to marhsal to/from JSON
type tradeJSON struct {
	Id   int     `json:"trade_id"`
	Time float64 `json:"pv_time"`
}

// MarhsalJSON returns the JSON form of this tradePVTimes instance
func (t tradePVTimes) MarshalJSON() ([]byte, error) {
	trades := []tradeJSON{}
	for i := 1; i <= len(t); i++ {
		trades = append(trades, tradeJSON{i, t[tradeId(i)]})
	}

	return json.Marshal(trades)
}

// createTradePVs returns an array of trades with random PV times as defined by the varieties
func createTradePVs(varieties []tradeVariety) tradePVTimes {
	log.Printf("Creating trades for %v varieties", len(varieties))

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	ret := make(tradePVTimes)

	tradeNum := 0
	for v := range varieties {
		log.Printf("%v trades with PV cost in range [%v, %v)", varieties[v].N, varieties[v].ShortestTime, varieties[v].LongestTime)
		for i := 0; i < varieties[v].N; i++ {
			tradeNum += 1
			ret[tradeId(tradeNum)] = varieties[v].ShortestTime + r.Float64()*(varieties[v].LongestTime-varieties[v].ShortestTime)
		}
	}

	log.Printf("Completed trade creation - %v created", len(ret))
	return ret
}

// tradePVSet represents a set of PV calculations for a trade
type tradePVSet struct {
	TradeId tradeId `json:"trade_id"`   // The id of the trade
	N       int     `json:"number_pvs"` // The number of PVs of this trade
}

// task is an internal structure representing a single task sent to a grid node
type task struct {
	TaskId   int          // The unique task id
	Step     *batchStep   // The step that this task has been created for
	Group    *group       // The group of nodes that this task will be dispatched to
	Trades   []tradePVSet // The set of trades and the number of their PVs being calculated
	TradeSet tradePVTimes // The set of trades from which Trades has been selected
}

// duration returns the total compute time of the trades in the task
func (t *task) duration() float64 {
	var tot float64
	for i := range t.Trades {
		tot = tot + t.TradeSet[t.Trades[i].TradeId]*float64(t.Trades[i].N)
	}
	return tot
}

type taskList []task

func (t taskList) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }
func (t taskList) Len() int           { return len(t) }
func (t taskList) Less(i, j int) bool { return t[i].duration() > t[j].duration() } // Reverse sort

// ttPV used for providing a sorted list of trades by PV time
type ttPV struct {
	Id tradeId
	T  float64
}

type ttPVList []ttPV

func (t ttPVList) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }
func (t ttPVList) Len() int           { return len(t) }
func (t ttPVList) Less(i, j int) bool { return t[i].T > t[j].T } // Reverse sort

// modelledGridActivity represents how tasks are processed on the grid
type modelledGridActivity map[int]map[int][]task

// elapsedTime looks at the work of each node to determine the longest time they will be working
// and the number of tasks that will be completed by this longest running node
func (m modelledGridActivity) elapsedTime() (float64, int) {
	var elapsedTime float64
	var taskCycles int
	for _, groupMap := range m {
		for _, nodeTasks := range groupMap {
			var tot float64
			for i := range nodeTasks {
				tot += nodeTasks[i].duration()
			}
			if tot > elapsedTime {
				elapsedTime = tot
				taskCycles = len(nodeTasks)
			}
		}
	}
	return elapsedTime, taskCycles
}

func (m modelledGridActivity) MarshalJSON() ([]byte, error) {

	o := make(map[string]interface{})

	type taskJSON struct {
		Id       int          `json:"task_id"`
		StepId   int          `json:"batch_step_id"`
		Duration float64      `json:"duration"`
		Trades   []tradePVSet `json:"trade_ids"`
	}

	type nodeJSON struct {
		Id    int        `json:"node_id"`
		Tasks []taskJSON `json:"tasks"`
	}

	type groupJSON struct {
		Id    int        `json:"group_id"`
		Nodes []nodeJSON `json:"nodes"`
	}

	groups := []groupJSON{}
	for i := 0; i < len(m); i++ {
		groupMap := m[i]
		nodes := []nodeJSON{}
		for j := 0; j < len(groupMap); j++ {
			tasks := []taskJSON{}
			for k := range groupMap[j] {
				task := groupMap[j][k]
				tasks = append(tasks, taskJSON{task.TaskId, task.Step.Id, task.duration(), task.Trades})
			}
			nodes = append(nodes, nodeJSON{j, tasks})
		}
		groups = append(groups, groupJSON{i, nodes})
	}

	o["groups"] = groups

	return json.Marshal(o)
}

/*
 MockTradeSet generates a set of trades based on the variety information provided.

 The variety information provided must be JSON in the form:

	[
	 	{
			"trade_count": 1000,
			"shortest_pv_time": 0.001,
			"longest_pv_time": 0.1
	 	}
	]

 The function generates a trade set with random PV times using these varieties, and returns
 JSON of the form:

 	[
 		{
			"trade_id": 1234,
			"pv_time": 0.092792133
 		}
 	]

*/
func MockTradeSet(b []byte) ([]byte, error) {

	// Extract varieties of trades
	varieties := []tradeVariety{}
	err := json.Unmarshal(b, &varieties)
	if err != nil {
		return nil, err
	}

	// Validate the data
	for i := range varieties {
		if varieties[i].N < 0 {
			return nil, errors.New(fmt.Sprintf("Error in number of trades for variety %v", i+1))
		}
		if varieties[i].LongestTime < 0.0 || varieties[i].ShortestTime < 0.0 || varieties[i].LongestTime <= varieties[i].ShortestTime {
			return nil, errors.New(fmt.Sprintf("Error in Longest/Shortest times for variety %v", i+1))
		}
	}

	// Build trade set and convert to JSON
	return json.Marshal(createTradePVs(varieties))
}

/*
	ModelGridActivity returns the grid processing expected to complete the supplied batch steps across the specified trade population

	The input information provided must be JSON in the form:

	{
		"duration":20,
		"batch_steps":
			[
				{"batch_step_id":0, "number_pvs":56}
			],
		"node_groups":
			[
				{"node_group_id":0, "node_count":1000, "performance_scaling":1.0}
			],
		"trade_set":
			[
				{"trade_id":1234, "pv_time":0.092792133}
			]
	}

	The function returns JSON, detailing by node group, what tasks have been assigned to each node and their duration and trades.  The
	function also provides summary information

	{
		"activity":
			{	"groups":
					[
						{	"group_id":0,
							"nodes":
								[
									{	"node_id":0,
										"tasks":
											[
												{	"task_id":0,
													"batch_step_id":0,
													"duration":19.99993189998909,
													"trade_ids":
														[36965,36591,33558,11694,42958,0]
												}
											]
									}
								]
						}
					]
			},
		"summary_data":
			{	"total_pv_time":113632.79596706692,
				"total_tasks":5682,
				"total_task_duration":113632.79596706676,
				"elapsed_time":119.99987970000603,
				"Cycles":6,
				"time_reduction_factor":946.9409157004434
			}
	}

*/
func ModelGridActivity(b []byte) ([]byte, error) {

	// Extract the input data into concrete types
	duration, groups, steps, tradePVs, err := func(b []byte) (float64, []group, []batchStep, tradePVTimes, error) {

		m := map[string]interface{}{}
		err := json.Unmarshal(b, &m)
		if err != nil {
			return 0.0, nil, nil, nil, err
		}

		i, ok := m["duration"]
		if !ok {
			return 0.0, nil, nil, nil, errors.New("Missing task duration")
		}
		taskDuration, ok := i.(float64)
		if !ok {
			return 0.0, nil, nil, nil, errors.New("Task duration is wrong type - should be float")
		}

		i, ok = m["batch_steps"]
		if !ok {
			return 0.0, nil, nil, nil, errors.New("Missing batch steps")
		}
		b1, err := json.Marshal(m["batch_steps"])
		if err != nil {
			return 0.0, nil, nil, nil, err
		}
		steps := []batchStep{}
		err = json.Unmarshal(b1, &steps)
		if err != nil {
			return 0.0, nil, nil, nil, err
		}
		if len(steps) == 0 {
			return 0.0, nil, nil, nil, errors.New("No batch steps specified - empty list")
		}
		b1, err = json.Marshal(m["node_groups"])
		if err != nil {
			return 0.0, nil, nil, nil, err
		}
		groups := []group{}
		err = json.Unmarshal(b1, &groups)
		if err != nil {
			return 0.0, nil, nil, nil, err
		}
		if len(groups) == 0 {
			return 0.0, nil, nil, nil, errors.New("No node groups specified - empty list")
		}
		b1, err = json.Marshal(m["trade_set"])
		if err != nil {
			return 0.0, nil, nil, nil, err
		}
		trades := []tradeJSON{}
		err = json.Unmarshal(b1, &trades)
		if err != nil {
			return 0.0, nil, nil, nil, err
		}
		if len(trades) == 0 {
			return 0.0, nil, nil, nil, errors.New("No trades specified - empty list")
		}
		tradePVs := map[tradeId]float64{}
		for j := range trades {
			tradePVs[tradeId(trades[j].Id)] = trades[j].Time
		}

		return taskDuration, groups, steps, tradePVs, nil
	}(b)

	if err != nil {
		return nil, err
	}

	if len(groups) > 1 {
		return nil, errors.New("Currently only 1 node group is modelled")
	}

	// Ensure node group scaling is relative to the fastest, which should have a scaling of one
	maxScaling := 0.0
	for i := range groups {
		if groups[i].Scaling > maxScaling {
			maxScaling = groups[i].Scaling
		}
	}
	if math.Abs(maxScaling) < 0.00001 {
		return nil, errors.New("Maximum group node scaling factor is too small")
	}

	for i := range groups {
		groups[i].Scaling = groups[i].Scaling / maxScaling
	}

	// Create the set of tasks needed to complete the steps
	tasks := createTasks(tradePVs, duration, steps, groups)

	// Model the grid activity based on the tasks
	gridActivity := make(modelledGridActivity)
	for i := range groups {
		group := groups[i]
		gridActivity[group.Id] = make(map[int][]task)
		for j := 0; j < group.Q; j++ {
			gridActivity[group.Id][j] = []task{}
		}
		// Determine tasks assigned to this group
		groupTasks := taskList{}
		for j := range tasks {
			task := tasks[j]
			if task.Group.Id == group.Id {
				groupTasks = append(groupTasks, task)
			}
		}
		// Sort reversing duration order to ensure long tasks get sent to nodes first
		sort.Sort(groupTasks)

		// Dispatch "round-robin" to each of the nodes in the group
		for k := range groupTasks {
			l := k % group.Q
			gridActivity[group.Id][l] = append(gridActivity[group.Id][l], groupTasks[k])
		}
	}

	// Get summary data
	elapsedTime, cycles := gridActivity.elapsedTime()
	totDuration := 0.0
	totTasks := 0
	for _, gm := range gridActivity {
		for _, ta := range gm {
			totTasks += len(ta)
			for i := range ta {
				t := ta[i]
				totDuration += t.duration()
			}
		}
	}

	// Summary return type for JSON marshalling
	type stats struct {
		TotalPVTime   float64 `json:"total_pv_time"`
		TotalTasks    int     `json:"total_tasks"`
		TotalDuration float64 `json:"total_task_duration"`
		ElapsedTime   float64 `json:"elapsed_time"`
		Cycles        int     `json:""cycles`
		Factor        float64 `json:"time_reduction_factor"`
	}

	// Construct return data
	ret := map[string]interface{}{
		"summary_data": stats{
			tradePVs.calculateTotalPVTime(steps),
			totTasks,
			totDuration,
			elapsedTime,
			cycles,
			totDuration / elapsedTime,
		},
		"activity": gridActivity,
	}

	// Convert return data to JSON
	return json.Marshal(ret)
}

// createTasks generates the tasks, given the trade PV and group node distributions
// This is the key function in the processing
func createTasks(tradePVs tradePVTimes, taskDuration float64, steps []batchStep, groups []group) []task {

	spvs := func(pvTimes tradePVTimes) []ttPV {

		p := make(ttPVList, len(pvTimes))
		i := 0
		for k, v := range pvTimes {
			p[i] = ttPV{k, v}
			i++
		}
		sort.Sort(p)
		return p
	}(tradePVs)

	tasks := []task{}

	group := groups[0] // Only assume one group for now

	toInt := func(f float64) int {
		return int(f + 0.0001)
	}

	taskId := 0
	for stepOffset := range steps {
		step := steps[stepOffset]
		normalisedDuration := taskDuration * group.Scaling

		type remainingInfo struct {
			N int
			T float64
		}
		remaining := make(map[tradeId]*remainingInfo)
		used := make(map[tradeId]bool)
		for len(used) < len(tradePVs) {
			trades := []tradePVSet{}
			var duration float64
			if len(remaining) > 0 {
				// There are some trades whose PVs are split across tasks.
				// Attempt to add their PVs into the next task
				completedTrades := map[tradeId]bool{}
				uncompletedTrades := map[tradeId]int{}
				for id, idInfo := range remaining {
					addablePVs, _ := math.Modf((normalisedDuration - duration) / idInfo.T)
					selectedPVs := math.Min(addablePVs, float64(idInfo.N))
					if numPVs := toInt(selectedPVs); numPVs > 0 {
						duration += selectedPVs * idInfo.T
						trades = append(trades, tradePVSet{id, numPVs})
						log.Printf("id %v, addable %v, N %v", id, numPVs, idInfo.N)
						if int(selectedPVs) == idInfo.N {
							completedTrades[id] = true
						} else {
							uncompletedTrades[id] = idInfo.N - numPVs
						}
					}
				}
				for id, _ := range completedTrades {
					log.Printf("Completed for %v", id)
					delete(remaining, id)
					used[id] = true
				}
				for id, n := range uncompletedTrades {
					log.Printf("%v PVs remain for for %v", n, id)
					remaining[id].N = n
				}
			}
			for i := range spvs {
				if normalisedDuration-duration < 0.01 {
					// Stop looking for small PVs at this point
					break
				}
				ttPV := spvs[i]
				if _, ok := remaining[ttPV.Id]; ok {
					continue // Ignore partially assigned trades
				}
				if _, ok := used[ttPV.Id]; !ok {
					if ttPV.T > normalisedDuration {
						// Trade calculation time is greater than the desired task duration, so each PV
						// is performed in a separate task (assume that these can be assembled later)
						log.Printf("Creating separate tasks for trade %v (PV is %v)", ttPV.Id, ttPV.T)
						for j := 0; j < step.N; j++ {
							tasks = append(tasks, task{taskId, &step, &group, []tradePVSet{tradePVSet{ttPV.Id, 1}}, tradePVs})
							taskId += 1
						}
						used[ttPV.Id] = true
					} else {
						if duration+float64(step.N)*ttPV.T <= normalisedDuration {
							// All PV calcs for this trade can be processed in the current task
							trades = append(trades, tradePVSet{ttPV.Id, step.N})
							duration += ttPV.T * float64(step.N)
							log.Printf("Can add all PVs for trade %v into task %v (PV is %v) (remaining time %v)", ttPV.Id, taskId, ttPV.T, normalisedDuration-duration)
							used[ttPV.Id] = true
						} else {
							if duration+ttPV.T <= normalisedDuration {
								// Can add at least one PV to this task - add as many as possible
								addablePVs, _ := math.Modf((normalisedDuration - duration) / ttPV.T)
								if numPVs := toInt(addablePVs); numPVs > 0 {
									duration += addablePVs * ttPV.T
									trades = append(trades, tradePVSet{ttPV.Id, numPVs})
									log.Printf("Added %v PVs for trade %v into task %v (PV is %v) (remaining time %v)", numPVs, ttPV.Id, taskId, ttPV.T, normalisedDuration-duration)
									remaining[ttPV.Id] = &remainingInfo{step.N - numPVs, ttPV.T}
								}
							}
						}
					}

				}
			}
			tasks = append(tasks, task{taskId, &step, &group, trades, tradePVs})
			taskId += 1
			log.Printf("total: %v, used: %v, remaining: %v", len(tradePVs), len(used), len(remaining))
		}
	}

	return tasks
}

func readBody(r *http.Request) ([]byte, error) {
	body := []byte{}

	length := 0
	var err error
	for length > 0 || (length == 0 && err != io.EOF) {
		buf := make([]byte, 512)
		length, err = r.Body.Read(buf)
		for i := 0; i < length; i++ {
			body = append(body, buf[i])
		}
	}
	if err != io.EOF {
		return nil, err
	}
	return body, nil
}

func errorToByte(err error) []byte {
	return []byte(fmt.Sprintf("%v", err))
}

func handleTradeSetRequest(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received tradeset request")
	defer log.Printf("Completed tradeset request")

	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	b, err := readBody(r)
	if err != nil {
		w.Write(errorToByte(err))
	} else {
		trades, err := MockTradeSet(b)
		if err != nil {
			w.Write(errorToByte(err))
		} else {
			w.Write(trades)
		}
	}
}

func handleGridActivityRequest(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received activity request")
	defer log.Printf("Completed activity request")

	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	b, err := readBody(r)
	if err != nil {
		w.Write(errorToByte(err))
	} else {
		trades, err := ModelGridActivity(b)
		if err != nil {
			w.Write(errorToByte(err))
		} else {
			w.Write(trades)
		}
	}
}

// Execution via webserver

func main() {

	DEFAULT_PORT := "8027"

	// Configure the standard logger
	log.SetFlags(log.Lmicroseconds | log.Ldate | log.Lshortfile)

	log.Println(">> gridmodeller")
	defer log.Println("<< gridmodeller")

	var port string
	if port = os.Getenv("PORT"); len(port) == 0 {
		log.Printf("Warning, PORT not set. Defaulting to %+v\n", DEFAULT_PORT)
		port = DEFAULT_PORT
	}

	http.HandleFunc("/tradeset", handleTradeSetRequest)
	http.HandleFunc("/activity", handleGridActivityRequest)

	log.Fatal(http.ListenAndServe(":"+port, nil))
}

// Alternate execution - command line test rather than via HTTP server

/*
func main() {

	createTestVarieties := func() ([]byte, error) {
		testTradeVarieties := []tradeVariety{
			{250000, 0.001, 0.1},
		}

		return json.Marshal(testTradeVarieties)
	}

	createTestGridActivityInputs := func(t []byte) ([]byte, error) {

		taskDuration := 20.0
		batchSteps := []batchStep{
			{0, 37},
			{1, 1},
		}
		groups := []group{
			{0, 1000, 1.0},
		}

		tradePVs := []tradeJSON{}
		err := json.Unmarshal(t, &tradePVs)
		if err != nil {
			return nil, err
		}

		inputs := map[string]interface{}{
			"duration":    taskDuration,
			"batch_steps": batchSteps,
			"node_groups": groups,
			"trade_set":   tradePVs,
		}

		return json.Marshal(inputs)
	}

	b, err := createTestVarieties()
	if err != nil {
		fmt.Printf("Error creating test varieties - %v", err)
		return
	}

	t, err := MockTradeSet(b)
	if err != nil {
		fmt.Printf("Error creating mock trade set - %v", err)
		return
	}

	i, err := createTestGridActivityInputs(t)
	if err != nil {
		fmt.Printf("Error creating mock inputs - %v", err)
		return
	}

	resp, err := ModelGridActivity(i)
	if err != nil {
		fmt.Printf("Error creating grid activity - %v", err)
		return
	}

	fmt.Println(string(resp))
}
*/
