grid-model
==========

#### Web-server based modeller for computational task dispatching to a grid of compute nodes ####

#### Motivation: ####

Demonstrate how to determine trade/PV assignments to tasks to minimise the elapsed time on a compute
grid whilst using a standard task duration when possible to simplify grid management.

The solution is presented as a web-server that accepts POST requests containing appropriately formed JSON bodies.

See the `example` folder to see the structure of the JSON objects.

The solution can also be loaded onto IBM Bluemix.  To do so (assuming a "go" BlueMix application called "gridmodeller" exists:

1. Move to the folder `cd gridmodeller`
2. Ensure the link to app.go is created `ln ../app.go`
3. Login into BlueMix via the cf CLI
4. Run the command `cf push gridmodeller`

You should now be able to modify the `example/gen_request` script to point to the BlueMix endpoint, and send requests.
